import React from 'react';
import '../Css/Navbar.css'

const Navbar = () => {
    return (
        <div className="navigation-bar">
          <div className="div-2">
            <div className="overlap">
              <div className="text-wrapper-2">Documentation</div>
            </div>
            <div className="group-3" componentHeaderClassName="group-instance" />
            <div className="text-wrapper-3">Contact</div>
            <img className="connectified-logo" alt="Connectified logo" src="connectified-logo-2-1.png" />
            <div className="text-wrapper-4">Services</div>
            <div className="text-wrapper-5">Partners</div>
          </div>
        </div>
      );
};

export default Navbar;
