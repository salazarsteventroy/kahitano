import React from "react";
import "../../Css/HomeCss/Header.css";

const Header = ({ headerClassName }) => {
    return (
        <div className="component">
          <div className="overlap-group">
            <div className="browser">
              <div className="button" />
              <div className="div" />
              <div className="button-2" />
            </div>
            <div className={`header ${headerClassName}`}>
              <p className="get-it-right-first">
                Get it right, first time,
                <br />
                every time.
              </p>
              <p className="text-wrapper">
                Regardless of where you are in your IoT journey, using the right IoT solutions approach from the get-go will
                provide peace-of-mind and give your business the confidence to make targeted time and effort investments
                that will lead to quicker payback and provide a better return.
              </p>
            </div>
          </div>
        </div>
  );
};

export default Header;
