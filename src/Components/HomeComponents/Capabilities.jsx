import React from 'react';
import '../../Css/HomeCss/Capabilities.css'

const Capabilities = () => {
    return (
        <div className="group">
          <div className="overlap">
            {/* <div className="text-wrapper">Title 02</div> */}
            <div className="rectangle" />
          </div>
          <div className="overlap-group">
            <p className="our-capabilities">
              Our Capabilities:
              <br />
              Bringing IOT to Life
            </p>
            <div className="div" />
            <div className="text-wrapper-2">See our Capability Statement</div>
          </div>
          <p className="p">
            IoT is more than the sum of its components. As you power along your IoT journey, let our team do the hard work
            to make sure it works seamlessly, first time, every time for the life of the initiative.
          </p>
          <div className="overlap-2">
            <div className="text-wrapper-3">Title 01</div>
            <div className="rectangle-2" />
          </div>
          <div className="overlap-group-2">
            <div className="text-wrapper-4">Title 04</div>
            <div className="rectangle-3" />
          </div>
          <div className="overlap-3">
            <div className="text-wrapper-5">Title 03</div>
            <div className="rectangle-4" />
          </div>
        </div>
      );
  };
  
  export default Capabilities;