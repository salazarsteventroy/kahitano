import React from "react";
import '../../Css/HomeCss/AlreadyHaveIot.css'

const AlreadyHaveIoT = () => {
  return (
    <div className="group2">
      <div className="text-wrapper2">Already have IoT?</div>
      <p className="div2">
        We can help to get you connected onto the cellular LTE network with the
        peace of mind your solution will work as intended and importantly within
        budget.
      </p>
      <div className="overlap2">
        <div className="text-wrapper-22">Get Connected</div>
      </div>
      <div className="overlap-group2">
        <div className="text-wrapper-32">Free-of-cost SIM test kits</div>
        <img
          className="icon-SIM-card2"
          alt="Icon SIM card"
          src="https://c.animaapp.com/KJSSoIVG/img/---icon--sim-card--1@2x.png"
        />
        <div className="rectangle2" />
      </div>
      <div className="overlap-22">
        <div className="text-wrapper-42">Bulk SIM Purchasing</div>
        <img
          className="icon-box-open2"
          alt="Icon box open"
          src="https://c.animaapp.com/KJSSoIVG/img/---icon--box-open--1@2x.png"
        />
        <div className="rectangle-22" />
      </div>
      <div className="overlap-32">
        <div className="text-wrapper-52">No roaming latency</div>
        <img
          className="icon-lightning-fill2"
          alt="Icon lightning fill"
          src="https://c.animaapp.com/KJSSoIVG/img/---icon--lightning-fill--1@2x.png"
        />
        <div className="rectangle-32" />
      </div>
      <div className="overlap-42">
        <div className="text-wrapper-62">Guaranteed low rates</div>
        <img
          className="vector2"
          alt="Vector"
          src="https://c.animaapp.com/KJSSoIVG/img/vector-1.svg"
        />
        <div className="rectangle-42" />
      </div>
    </div>
  );
};

export default AlreadyHaveIoT;
