import React from "react";
import Navbar from "../Components/Navbar";
import Header from "../Components/HomeComponents/Header";
import Capabilities from "../Components/HomeComponents/Capabilities";
import "../Css/HomeCss/Header.css";
import AlreadyHaveIoT from "../Components/HomeComponents/AlreadyHaveIoT";

const Home = () => {
  return (
    <div className="landingContainerOne">
      <Navbar />
      <Header />
      <Capabilities />
      <AlreadyHaveIoT/>
    </div>
  );
};

export default Home;
